#!/bin/bash

MOUNT=/home/u061718/mount

CONDA_PYTHON_VERSION='3'
CONDA_BASE_PACKAGE='anaconda'
CONDA_VERSION='2021.05'

CONDA_BASE_PACKAGE_NAME=`echo $CONDA_BASE_PACKAGE | sed -r 's/\<./\U&/g'` \
    && OS=`uname -s` \
    && ARCH=`uname -m` \
    && CONDA_INSTALL_PACKAGE=$CONDA_BASE_PACKAGE_NAME$CONDA_PYTHON_VERSION-$CONDA_VERSION-$OS-$ARCH.sh \
    && echo "installing $CONDA_INSTALL_PACKAGE" \
    && wget -q --show-progress --progress=bar:force:noscroll https://repo.anaconda.com/archive/$CONDA_INSTALL_PACKAGE -O $CONDA_BASE_PACKAGE.sh \
    && bash $CONDA_BASE_PACKAGE.sh -b -p $MOUNT/tool/python/$CONDA_BASE_PACKAGE$CONDA_PYTHON_VERSION \
    && rm $CONDA_BASE_PACKAGE.sh

# update .bashrc
#$MOUNT/tool/python/$CONDA_BASE_PACKAGE$CONDA_PYTHON_VERSION/bin/conda init bash

# update .condarc
#$MOUNT/tool/python/$CONDA_BASE_PACKAGE$CONDA_PYTHON_VERSION/bin/conda config --set auto_activate_base false

mkdir -p $MOUNT/tool/python/conda/env
#$MOUNT/tool/python/$CONDA_BASE_PACKAGE$CONDA_PYTHON_VERSION/bin/conda config --add envs_dirs $MOUNT/tool/python/conda/env
