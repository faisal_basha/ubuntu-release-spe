export MOUNT=/home/$USER/mount

# create folders and change ownership
sudo mkdir -p $MOUNT/backup $MOUNT/build $MOUNT/data $MOUNT/project $MOUNT/tool
sudo chown -R developer:developers $MOUNT
sudo chmod -R g+rw $MOUNT
