#!/bin/bash

sudo apt-get update -y
sudo apt-get install clamav clamav-demon
sudo clamscan --version
sudo systemctl stop clamav-freshclam
sudo mkdir /var/lib/clamav
sudo wget http://smt.bastion.saab.ae/clamav/daily.cld -O-
sudo cp -r daily.cld /var/lib/clamav/daily.cld
sudo systemctl start clamav-freshclam
