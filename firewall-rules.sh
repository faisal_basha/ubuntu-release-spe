#!/bin/bash

echo "resetting iptables...."

iptables -F

###### LoopBack
iptables -A INPUT -i lo -m comment --comment "Allow loopback connections" -j ACCEPT
iptables -A OUTPUT -o lo -m comment --comment "Allow loopback connections" -j ACCEPT

###### DNS
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p icmp -m comment --comment "Allow Ping to work as expected" -j ACCEPT
iptables -A INPUT -p tcp  -m multiport --destination-ports 22,25,53,80,443,465,5222,5209,5280,8999:9003 -j ACCEPT
iptables -A INPUT -p udp  -m multiport --sport 53 -j ACCEPT
iptables -A INPUT -p tcp  -m multiport --sport 53 -j ACCEPT
iptables -A INPUT -s 10.183.0.0/16 -j ACCEPT
iptables -P INPUT DROP
iptables -A OUTPUT -d 10.183.0.0/16 -j ACCEPT
iptables -A OUTPUT -p tcp -d 127.0.0.1 -j ACCEPT
iptables -P OUTPUT DROP
iptables -P FORWARD DROP
